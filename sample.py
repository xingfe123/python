#!/usr/bin/env python
# Author : luoxing (xingfe123@gmail.com)
# License: GPL

import math
from random import uniform


def isPrimerFor(i, primers):
    for x in primers:
        if i % x == 0:
            return False
    return True


def primer(n):
    primers = set()
    i = n
    while True:
        if isPrimerFor(i, primers):
            primers.add(i)
            yield i


def sdnorm(z):
    """
    Standard normal probability density function
    """
    return math.exp(-z * z / 2.0) / math.sqrt(2 * math.pi)


if __name__ == '__main__':
    numbers = 1000
    alpha = 1
    innov = uniform(-alpha, alpha, numbers)

    x = 0.0
    vec = []
    for i in range(1, numbers):
        candidate = x + innov[i]
        acceptable = min(1, sdnorm(candidate) / sdnorm(x))
        u = uniform(0, 1)
        if u < acceptable:
            x = candidate
            vec.append(x)
