#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import pandas
import datetime
import math
import pandas.io.data


def discount(futureValue, rate, number):
    return futureValue / (1 + rate)**number


class Annuity(object):
    def __init__(self, firstValue, discount):
        self.firstValue = firstValue
        self.discount = discount

    def presentValue():
        return 0


class growthAnnuity(Annuity):
    def __init__(self, firstPeriod, growth, discount):
        self.firstValue = firstPeriod
        self.growth = growth
        self.discount = discount

    def discount(self):
        if self.growth < self.discount:
            return -1
        else:
            discount(self.firstValue, self.rate, self.number)


class EqualAnnuity(Annuity):
    def __init__(self, firstValue, discount, number):
        super().self.__init__(firstValue, discount)
        self.number = number

    def presentValue(self):
        return self.firstValue * (
            1 - (1 / (1 + self.discount))**self.number) / self.discount


class Found(object):
    def __init__(self, value):
        pass


class PerpetualAnnuity(Annuity):
    def __init__(firstValue, discount):
        super().self.__init__(firstValue, discount)

    def presentValue(self):
        return self.firstValue / self.discount


def effectiveAnnualRate(rate, frequency):
    return (1 + (rate / frequency))**frequency - 1


def continuousRate(rate, frequency):
    return frequency * math.log(1 + rate / frequency)


def PerpetualAnnuity(firstValue, discount, growth):
    return firstValue / (discount - growth)


def netPresentValue(rate, flows):
    total = 0.0
    for i, cash in enumerate(flows):
        total += cash / (1 + rate)**i


def paybackPeriod(rate, flows):
    total = 0.0
    for i, cash in enumerate(flows):
        total += cash / (1 + rate)**i
        if total > firstValue:
            return i


def Rate(cashflows):
    rate = 1.0
    invest = cashflows[0]
    for i in range(100):
        rate *= (1 - netPresentValue(rate, cashflows) / invest)


def netValue(benefit, cost):
    return benefit - cost


if __name__ == '__main__':
    # weeks and taking an opposite position
    # Liquidity is defined as how quickly we can dispose of our asset
    # without losing its intrinsic value

    # illiguidity
    # pastor and Stambaugh

    # Famma-French tree-factor model

    # Famma-MacBeth regression

    # Estimating rolling beta

    # Black Scholes Merton Option Model

    labels = ['a', 'b', 'c', 'd', 'e']
    seriers = pandas.Seriers([1, 2, 3, 4, 5], index=labels)
    mapping = seriers.to_dict()

    apple = pandas.io.data.get_data_yahoo(
        'AAPL',
        start=datetime.datetime(2006, 10, 1),
        end=datetime.datetime(2007, 10, 1))
    # Moving Average
