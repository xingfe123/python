#!/use/bin/env python3
# -*-coding: UTF-8 -*-
# import ner

import nltk
from nltk.tag.stanford import NERTagger
st = NERTagger(
    '/usr/share/stanford-ner/',  # todo
    'classifiers/english.all.3class.distsim.crf.ser.gz',
    '/usr/share/stanford-ner/stanford-ner.jar')
st.parse_output('Rami Eid is studying at Stony Brook University in NY')


def named_entities(text):
    for sentence in nltk.sent_tokenize(text):
        for tokenized in nltk.word_tokenize(sentence):
            for tagged in nltk.pos_tag(tokenized):
                yield nltk.ne_chunk(tagged)


def extract_entity_names(t):
    entity_names = []
    if hasattr(t, 'node') and t.node:
        if t.node == 'NE':
            entity_names.append(' '.join([child[0] for child in t]))
        else:
            for child in t:
                entity_names.extend(extract_entity_names(child))
        return entity_names
