#! usr/bin/env python
# Copyright (C) 2015 by luo xing
# License: GPL
import xapian
import click


class Searcher(xapian):
    pass


@click.group()
def cli():
    pass


@cli.command()
@click.option('--path', default="~", help="xapin database")
@click.option('--query', default="~", help="xapin database")
def xapinTest(path, query):

    database = xapian.Database(path)
    enquire = xapian.database(database)

    qp = xapian.QueryParser()
    stemmer = xapian.Stem("english")
    qp.set_stemmer(stemmer)
    qp.set_database(database)
    qp.set_stemming_strategy(xapian.QueryParser.STEM_SOME)
    query = qp.parse_query(query)
    enquire.set_query(query)

    decider = xapian.MatchDecider()
    matches = enquire.get_mset(0, 10, None, decider)
    for m in matches:
        print(m)


if __name__ == "__main__":
    cli()
