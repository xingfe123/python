from collections import Counter


def probabilityClassify(dataSeq):
    """
    >>> dataSeq = [[1,1,True],[1,0,True],[1,0,False],[0,1,False] ,[0,1,False]]
    >>> ln(2.7)
    
    >>> entropy(probabilityClassify(dataSeq))
    0.9709505944546686
    """
    from collections import Counter
    labels = [feat[-1] for feat in dataSeq]
    for key in Counter(labels):
        probability = Counter(labels)[key] / len(dataSeq)
        yield probability


def filterDataSeq(dataSeq, anxis, value):
    """
    >>> dataSeq = [[1,1,True],[1,0,True],[1,0,False],[0,1,False] ,[0,1,False]]
    >>> filterDataSeq(dataSeq,1,0)
    [[1, True], [1, False]]
    >>> filterDataSeq(dataSeq,1,1)
    [[1, True], [0, False], [0, False]]
    >>> filterDataSeq(dataSeq,0,0)
    [[1, False], [1, False]]
    """
    reData = []
    for feat in dataSeq:
        if feat[anxis] == value:
            # feat.pop(anxis)
            featV = feat[:anxis]
            featV.extend(feat[anxis + 1:])
            reData.append(featV)
            # reData.append(feat)
    return reData


def splitDataSeqFre(dataSeq, i):
    """
    >>> dataSeq = [[1,1,True],[1,0,True],[1,0,False],[0,1,False] ,[0,1,False]]
    >>> splitDataSeqFre(dataSeq,0)
    ([[[1, False], [1, False]], [[1, True], [0, True], [0, False]]], [0, 1])
    """
    if i < len(dataSeq[0]):
        l = []
        v = []
        unique = set([x[i] for x in dataSeq])
        for val in unique:
            subSeq = filterDataSeq(dataSeq, i, val)
            # yield subSeq
            l.append(subSeq)
            v.append(val)
        return l, v
    return None


def bestFeatureToSplit(dataSeq):
    """
    >>> dataSeq = [[1,1,True],[1,0,True],[1,0,False],[0,1,False] ,[0,1,False]]
    >>> bestFeatureToSplit(dataSeq)
    0
    """
    entropy = probabilityClassify(dataSeq)
    infoGain = {}
    for i in range(len(dataSeq[0]) - 1):
        # print(i)
        newEntropy = 0.0
        for subSeq, freq in splitDataSeq(dataSeq, i):
            prob = len(subSeq) / len(dataSeq)
            newEntropy += prob * probabilityClassify(subSeq)
        infoGain[i] = entropy - newEntropy
    re = sorted(infoGain.keys(), key=lambda x: infoGain[x], reverse=True)
    return re[0]


def splitDataSeq(dataSeq, i):
    """
    >>> dataSeq = [[1,1,True],[1,0,True],[1,0,False],[0,1,False] ,[0,1,False]]
    >>> splitDataSeq(dataSeq,0)
    [[[1, False], [1, False]], [[1, True], [0, True], [0, False]]]
    """
    if i < len(dataSeq[0]):
        l = []
        unique = set([x[i] for x in dataSeq])
        for val in unique:
            subSeq = filterDataSeq(dataSeq, i, val)
            # yield subSeq
            l.append(subSeq)
        return l
    return None


def majority(List):
    """
    >>> List = [1,1,2,3,4,]
    >>> majority(List)
    1
    """

    c = Counter(List)
    result, count = c.most_common(1)[0]
    return result


def createTree(dataSeq, labels):
    """
    >>> dataSeq = [['yes'],['yes']]
    >>> labels = []
    >>> createTree(dataSeq,labels)
    'yes'
    >>> dataSeq = [['yes'],['yes'],['No']]
    >>> createTree(dataSeq,labels)
    'yes'
    >>> dataSeq = [[1,1,True],[1,0,True],[1,0,False],[0,1,False] ,[0,1,False]]
    >>> labels = ['no surfacing', 'filppers']
    >>> createTree(dataSeq,labels)
    no surfacing
    """
    classList = [example[-1] for example in dataSeq]
    if len(set(classList)) == 1:
        return classList[0]
    if len(dataSeq[0]) == 1:
        return majority(classList)
    feature = bestFeatureToSplit(dataSeq)
    label = labels[feature]
    print(label)
    mytree = {label: {}}
    del (labels[feature])

    for subData, value in splitDataSeqFre(dataSeq, feature):
        subLabels = labels[:]
        mytree[label][value] = createTree(subData, subLabels)

    return mytree


if __name__ == '__main__':
    import doctest
    doctest.testmod()
