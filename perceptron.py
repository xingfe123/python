#! /usr/bin/env python3
# Copyright (C) 2015 by luo xing
# License: GPL

import numpy
import random
import math
from numpy import array, dot


def f(x, w, b):
    return float(b) + dot(x, w)


def f1(x, w, b):
    return math.exp(f(x, w, b))


def makearray(data):
    x = array([0.0, 0.0, 0.0, 0.0])
    for i, d in enumerate(data):
        x[i] = math.log(float(d.split(":")[1]))

    return x


def getPerceptron(r):
    w = array([0.25, 0.25, 0.25, 0.25])
    b = -0.25
    for x, label in r:
        # print(x,label)
        value = f1(x, w, b)
        # print(value)
        if value * label < 0:
            r = random.random()
            if label > 0:
                w += r * x
            else:
                w -= r * x
            b += r * label
            s = sum(w)
            #print(s)
            w = w / s
    print(w, b, value)
    return w, b


def generateData():
    with open('/home/luoxing/out.txt') as f:
        for line in f:
            data = line.split()
            y = int(data[0])
            x = makearray(data[1:])
            yield x, y


def main():
    return getPerceptron(generateData())


def do_some():
    tx = array([0.0] * 4)
    tb = 0.0
    for _ in range(100):
        x, b = main()
        tx += x
        tb += b
    print("ok,", tx, tb)
