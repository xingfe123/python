#!/use/bin/env python3
# -*-coding: utf-8 -*-
import sklearn
import sklearn.cluster


class ClusterModel(object):
    def score(self, data):
        return {}

    def fit():
        raise Exception("Method not implemented!")

    def labels():
        raise Exception("Method not implemented!")


class KMeanClusteringModel(ClusterModel):
    def __init__(self, number, init='k-means++', max_iter=100, n_init=1):
        self.number = number
        self.model = sklearn.cluster.KMeans(
            n_clusters=number, init=init, max_iter=max_iter, n_init=n_init)

    def fit(self, data):
        self.model.fit(data)

    def labels(self, threshold=None):
        return self.model.labels_

    def score(self, data):
        return {
            'value':
            sklearn.metrics.silhouette_score(
                data, self.model.labels_, metric='euclidean')
        }
