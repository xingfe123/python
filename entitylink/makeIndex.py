#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
###############################################################################
import xapian
from progressbar import ProgressBar
import entitylink.db as entitydb
from entity import extract


##############################################################################
def makeIndex(dbpath='/home/luoxing/windows/xapian-database6',
              xmlfile='/home/luoxing/windows/enwiki.xml'):
    db = xapian.WritableDatabase(dbpath, xapian.DB_CREATE_OR_OPEN)
    indexer = xapian.TermGenerator()
    indexer.set_stemmer(xapian.Stem("english"))
    pbar = ProgressBar(maxval=entitydb.getTitleNumber())
    for number, (id, content) in enumerate(extract.getNormalPage(xmlfile)):
        pbar.update(number + 1)
        if content is None:
            print(id)
            continue
        doc = xapian.Document()
        doc.set_data(content)
        doc.add_value(0, id)
        indexer.set_document(doc)
        indexer.index_text(content)
        db.add_document(doc)
    db.close()


###############################################################################

###############################################################################
if __name__ == '__main__':
    makeIndex()
