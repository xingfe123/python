#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

from entitylink.db import dumpId
from entitylink.extract import getTitleId
import sys

if __name__ == '__main__':
    i = 0
    size = dumpId.dbsize()
    for x, y in getTitleId('/home/luoxing/windows/enwiki.xml'):
        i += 1
        sys.stdout.write(str(i)+'\r')
        if i <= size:
            continue
        dumpId.set(x, y)
        
    
