#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

from entitylink.db import dumpText, mongo_set
from entitylink.extract import getTextId
import sys


def main(path):
    for i, (x, y) in enumerate(getTextId('/home/luoxing/windows/enwiki.xml')):
        sys.stdout.write(str(i+1)+'\r')
        try:
            mongo_set(dumpText, x, y)
        except Exception as e:
            print(e)
            

if __name__ == '__main__':
    main('/home/luoxing/windows/enwiki.xml')
