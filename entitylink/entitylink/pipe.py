#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
import logging


class Pipe(object):
    def __init__(self, previous, fun):
        self.fun = fun
        self.previous = previous
        if previous is not None:
            self.head = previous.head
            
    def process(self, elems):
        if self.previous is not None:
            elems = self.previous.process(elems)
        for x in self.fun.eval(elems):
            yield x

            
class Function(object):
    def eval(self, elems):
        return elems

            
class Mapper(Function):
    def eval(self, elems):
        for elem in elems:
            yield self.map(elem)
            
    def map(self, elem):
        return elem
    

class Filter(Function):
    def eval(self, elems):
        for elem in elems:
            if not self.isFilter(elem):
                yield elem
            
    def isFilter(self, elem):
        return False


class AsciiFilter(Filter):
    def isFilter(self, spot):
        isAscii = True
        if isAscii:
            return False
        logging.debug("spot %s contains not ascii char, filtering ", spot)
        return True
    
    
class ImageFilter(Filter):
    def isFilter(self, spot):
        return spot.contains("image:")
    

class CommonnessFilter(Filter):
    def __init__(self, threshold):
        self.threshold = threshold
        
    def isFilter(self, spot):
        entities = []
        for e in spot.getEntities():
            common = spot.getEntityCommonness(e)
            if common > self.threshold:
                entities.push(e)
            else:
                logging.debug()
        return entities.empty()

    
class TemplateFilter(Filter):
    def isFilter(self, spot):
        return spot.contains("template")


class SymbolFilter(Filter):
    def isFilter(self, spot):
        alpha = spot.matches(".*[a-z].*")
        if alpha:
            return False
        logging.debug(" spot %s does not contains alphabetic char, filter",
                      spot)

        
class ProbabilityFilter(Filter):
    def __init__(self, threshold):
        self.threshold = threshold
        
    def isFilter(self, spot):
        if spot is None:
            return True
        return spot.getLikProbability() <= self.threshold

    
class FrequrncyFilter(Filter):
    def isFilter(spot):
        return spot.getEntities().size() == 0


class LengthFilter(Filter):
    def __init__(self, min=3):
        self.min = min

    def isFilter(self, spot):
        return spot.length() < min

    
class LongSpotFilter(Filter):
    def isFilter(self, spot):
        count = 0
        len = spot.length()
        i = 0
        while i < len:
            if spot[i] == ' ':
                count += 1
            if count >= self.maxNumberOfTerms:
                return True

        return False
    
            
class NumberFilter(Filter):
    def isFilter(spot):
        spot = spot.replaceAll("[-+,.]", "")
        if spot.isdigit():
            return False
        logging.debug(" spot %s is a number, filtering", spot)
        return True
    
