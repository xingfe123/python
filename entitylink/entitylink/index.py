#!/usr/bin/env python3
import xapian
from progressbar import ProgressBar
import entitylink.db as entitydb
import itertools
import os


class PostingSource(xapian.PostingSource):
    def __init__(self, db, source):
        xapian.PostingSource.__init__(self)
        self.db = db
        self.source = source

    def get_termfreq_min(self):
        return 0
    
    def get_termfrew_max(self):
        return self.db.get_doccount()
    

class TermGenerator(xapian.TermGenerator):
    def __init__(self):
        xapian.TermGenerator.__init__(self)
        super().set_stemmer(xapian.Stem("english"))

    def index_text(self, content):
        super().index_text(content)


xapian_page_title = 0


class Database(object):
    def _init__(self, path):
        self.db = xapian.WritableDatabase(path, xapian.DB_CREATE_OR_OPEN)
        self.have = 0

    def add_page(self, title, content):
        " title and content is not None"
        doc = xapian.Document()
        doc.set_data(content)
        doc.add_value(xapian_page_title, title)
        self.db.add_document(doc)
        self.have += 1
        return doc
    
###############################################################################


##############################################################################
def makeIndex(dbpath='/home/luoxing/windows/xapian-database4',
              xmlfile='/home/luoxing/windows/enwiki.xml'):
    db = xapian.WritableDatabase(dbpath, xapian.DB_CREATE_OR_OPEN)
    indexer = xapian.TermGenerator()
    indexer.set_stemmer(xapian.Stem("english"))
    # start = 5589734
    # start = 5593535
    # start = 7389988
    # start = 7389988
    pbar = ProgressBar(maxval=entitydb.getTitleNumber())
    for number, (title, content) in enumerate(extract.getPages(xmlfile)):
        pbar.update(number+1)
        if content is None:
            print(title)
            continue
        doc = xapian.Document()
        doc.set_data(content)
        doc.add_value(xapian_page_title, title)
        indexer.set_document(doc)
        indexer.index_text(content)
        db.add_document(doc)
###############################################################################

###############################################################################

class TitleSearch(object):
    def __init__(self, dbpath="/home/luoxing/windows/xapian-title"):
        self.dbpath = dbpath
        self.db = xapian.WritableDatabase(dbpath, xapian.DB_OPEN)
        self.stem = xapian.Stem("english")
        self.enquire = xapian.Enquire(self.db)
        self.parser = xapian.QueryParser()
        self.parser.set_stemmer(self.stem)
        self.parser.set_database(self.db)
        
    def query_str(self, s):
        self.enquire.set_query(self.parser.parse_query(s))

    def get_mset(self, start, number):
        ms = self.enquire.get_mset(start, number)
        return ms
        
    def __del__(self):
        os.unlink(self.dbpath+'/flintlock')

class Searcher(object):
    def __init__(self, dbs=["/home/luoxing/xapian-database1",
                            "/home/luoxing/xapian-database3"]):
        self.stem = xapian.Stem("english")
        self.dbs = [xapian.WritableDatabase(x, xapian.DB_CREATE_OR_OPEN)
                    for x in dbs]
        self.enquires = [xapian.Enquie(x) for x in self.dbs]
        self.parsers = [xapian.QueryParser()]*len(self.dbs)
        
    def get_doccount(self):
        return sum([x.get_doccount() for x in self.dbs])

    def set_query_str(self, s):
        for parser, enquire in zip(self.parsers, self.enquires):
            enquire.set_query(parser.parse_query(s))

    def get_mset(self, offset, limit):
        self.msets = MSet([x.get_mset() for x in self.enquires])
        # return self.msets
        return None


class MSet(object):
    def __init__(self, msets):
        self.msets = msets

    def __iter__(self):
        return itertools.chain.from_iterable(self.msets)

    def size(self):
        return sum([x.size() for x in self.sets])
    
    def empty(self):
        return all([x.empty() for x in self.sets])

    def get_termfreq(self, s):
        return sum([x.get_termfreq(s) for x in self.sets])

    def get_termweight(self, s):
        return self.sets[1].get_termweight(s)
    

def sampleSearch(s):
    searcher = Searcher()
    searcher.set_query_str(s)
    matches = searcher.get_mset()
    for match in matches:
        printMatch(match)
    print('Number of documents returned: {}'.format(matches.size()))


def printMatch(match):
    print('===================')
    print('rank=%s, documentID=%s' % (match.rank, match.docid))
    print('-------------------')
    print(match.document.get_doc())
    print('===================')


def search(queryStr):
    dbPath1 = "/home/luoxing/xapian-database1"
    dbPath2 = "/home/luoxing/xapian-database2"
    db1 = xapian.WritableDatabase(dbPath1, xapian.DB_CREATE_OR_OPEN)
    db2 = xapian.WritableDatabase(dbPath2, xapian.DB_CREATE_OR_OPEN)
    queryPaser1 = xapian.QueryParser()
    queryPaser1.set_stemmer(xapian.Stem('english'))
    queryPaser1.set_database(db1)

    query = queryPaser1.parse_query(queryStr)
    enquire = xapian.Enquire(db)
    enquire.set_query(query)
    offset = 0
    limit = db.get_doccount()
    matches = enquire.get_mset(offset, limit)
    for match in matches:
        printMatch(match)
    print('Number of documents returned: {}'.format(matches.size()))


def otherTest():
    from whoosh.index import create_in
    from whoosh.fields import Schema, TEXT
    schema = Schema(title=TEXT(stored=True), content=TEXT(stored=True))
    ix = create_in("/home/luoxing/windows/indexdir", schema)
    xmlfile = '/home/luoxing/windows/enwiki.xml'
    writer = ix.writer()
    number = 0
    progress = ProgressBar(maxval= entitydb.getTotalPage())
    for title, content in extract.getPages(xmlfile):
        number += 1
        progress.update(number)
        if content:
            writer.add_document(title=title, content=content)
    progress.finsh()
    writer.commit()

if __name__ == '__main__':
    # main('/home/luoxing/xapian-database1')
    # main()
    makeIndex()
    #  otherTest()


