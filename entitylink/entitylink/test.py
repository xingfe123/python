#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL


import xapian
dbpath = "/home/luoxing/xapian-database1"
db = xapian.WritableDatabase(dbpath, xapian.DB_CREATE_OR_OPEN)
docid = db.get_doccount()
doc = db.get_document(docid)
print(docid)
db.close()
