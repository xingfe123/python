#/usr/bin/env python3 
# -*- coding: utf-8 -*-
import sys
import xapian


def query_print(searcher, query_string):
        pass


class Searcher(object):
    """"""
    def __init__(self, enquire, parse):
        "docstring"
        self.eq = enquire
        self.qp = parse
        
    def get_mset(self, str):
        self.eq.set_query(self.qp.parse_query(str))
        return self.eq.get_mset(0, 10)


class Enquire(xapian.Enquire):
    """"""
    def __init__(self, database):
        "docstring"
        xapian.Enquire.__init__(self, database)
        
        
class QueryParser(xapian.QueryParser):
    """
    Parse the query string to produce a Xapian::Query
    """
    
    def __init__(self, database):
        """
        my self 
        """
        xapian.QueryParser.__init__(self)
        self.set_stemmer(xapian.Stem("english"))
        self.set_database(database)
        self.set_stemming_strategy(xapian.QueryParser.STEM_SOME)


deault_db = xapian.Database("/home/luoxing/inferenceB")
                     
default_ir = \
             Searcher(xapian.Enquire(default_db), QueryParser(default_db))


if __name__ == "__main__":
    path = sys.argv[1]
    if len(sys.argv) < 2:
        print("Usage: %s path_to database query", sys.argv[0], file=sys.stderr)
    try:
        database = xapian.Database(path)
        query_string = "wiki"
        searcher = Searcher(xapian.Enquire(database), QueryParser(database))
        query_print(searcher, query_string)
        print("Parsed query is %s", query_string)

        matches = searcher.get_mset(query_string)
        
        print("%i results found.", matches.get_matches_estimated())
        print("Results 1-%i:", matches.size())
        for m in matches:
            print("%i: %i%% docid=%i [%s]",
                  m.rank+1, m.percent, m.docid, m.document.get_data())

    except Exception as e:
        print("Exception: %s ", str(e))
        sys.exit(1)
        
        

    
        
        

    
    

    
