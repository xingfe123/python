#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
import logging
import re
from UserString import MutableString
from spot import SpotManager


MAX_TERM_NUMBER = 100


def split_page(xmlfile):
    wiki = open(xmlfile)
    text_buffer = ""
    start = 0
    end = 0
    while True:
        chunk = wiki.read(16*1024*1024)
        if chunk:
            text_buffer += chunk
        start = 0
        end = 0
        while True:
            start = text_buffer.find("<page>", start)
            if start == -1:
                break
            end = text_buffer.find("</page", end)
            if end == -1:
                break
            yield text_buffer[start:end+len("</page>")]

            start = end + len("</page>")
            end = start
        if chunk == "":
            break
        if start_index == -1:
            text_buffer = ""
        else:
            text_buffer = text_buffer[start_index:]




class ContextExtractor(object):
    logger = None
    positions = []
    ignored_tag = []
    
    @staticmethod
    def normalize(title):
        title = title.strip('\s_')
        title = re.sub(r'[\s_]+', ' ', title)
        return title
    
    @staticmethod
    def isPunctation(i):
        return i == ' ' or i == '.' or i == ',' or i == ';'

    def __init__(self, text):
        self.text = text
        self.Wsize = 50
        self.init(text)
        
    def init(self, text):
        len = text.len()
        self.positions.add(0)
        i = 0
        while i < len:
            if text[i] == ' ':
                self.positions.add(i)
                i += 1
                while i < len and self.isPunctation(text[i]):
                    i += 1
        self.positions.add(len)
    
    def _cleanContext(self):
        context = self.text.replaceAll("[\\[\\]]")
        return context
    
    def _termPos(self, pos):
        return self._closest(pos, self.positions)
    
    def _getContext(self, l, r):
        start = max(0, 1-self.Wsize/2)
        end = min(self.positions.size()-1, r+self.Wsize/2)
        return self.text.substring(self.positions[start], self.positions[end])
    
    def getContext(self, label):
        buffer = MutableString()
        terms = 0
        i = self.text.indexOf(label, 0)
        while i != -1:
            buffer += self._getContext(
                self._termPos(i), self._termPos(i+label.length()+1))
            buffer += " "
            terms += self.Wsize
            if terms >= MAX_TERM_NUMBER:
                break
            i = self.text.indexOf(label, i+1)
        context = self._cleanContext(buffer)
        return context.trim()
    
    def _closest(key, lst):
        size = lst.size()
        delta = size/2
        i = size/2
        while delta > 1:
            logging.debug('i = %s \t delta = {} ', i, delta)
            elem = lst[i]
            delta = delta/2 + delta % 2
            if elem == key:
                return i
            if elem > key:
                i = max(i-delta, 0)
            else:
                i = min(delta+i, size-1)

        elem = lst[i]
        if elem > key:
            return i-1
        return i
    

class ArticleContextExtractor(ContextExtractor):

    cleaner = SpotManager.getStandardSpotCleaner()
    buffer = None
    
    def __init__(self, article):
        self.text = self.getCleanAsciiText(article)
        
    def getCleanAsciiText(self, article):
        if self.cleanText is not None:
            return self.cleanText
        self.buffer = article.getTitle().append(". ")
        for para in article.getParagraphs():
            self.buffer.append(self.cleaner.clean(para))
            self.buffer.append(" ")
            
        for link in article.getLinks():
            self.buffer.append(link.getDescription())
            self.buffer.append(" ")

        self.cleanText = self.buffer.toString()
        return self.cleaText
    
