#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
import logging
import enum


class EntityType(enum.Enum):
    PER = 0
    UKN = 1
    GPE = 2
    ORG = 3
    
    @staticmethod
    def make(str):
        return EntityType[str]


class CandidateEntity(object):
    def __init__(self, entity, freq, commonness):
        self.entity = entity
        self.freq = freq
        self.commonness = commonness
        
    def __lt__(self, other):
        return self.commonness < other.commonness


class EntityRelatedness(object):
    def __init__(self, x, y, type):
        self.x = x
        self.y = y
        self.type = type
        self.relatedness = 0.0

        
class EntitySpots(object):
    def __init__(self, entity, wikiname, spots):
        self.entity = entity
        self.wikiname = wikiname
        self.spots = spots
        

class Entity(object):
    def __init__(self, id, frequency):
        self.id = id
        self.frequency = frequency

    def isDisambiguation(self, id):
        return id < 0

    
class EntityMatch(object):
    def __init__(self, entity, score, spot):
        self.entity = entity
        self.score = score
        self.spot = spot

    def __lt__(self, other):
        if self.score < other.score:
            return True
        return False
    
        
class EntityMatchList(list):
    def append(self, e):
        return super().append(e)
    
    def indexOf(self, id):
        pos = 0
        for em in self:
            if em.id() == id:
                return pos
            pos += 1
        return -1
    
    def normalize(self):
        total = 0.0
        for e in self:
            total += e.getScore()
        for e in self:
            e.score /= total
            

class EntityRanker(object):
    RANK_BY_PRIOR = True
    
    def __init__(self, field, threshold):
        if self.RANK_BY_PRIOR:
            logging.info("(e|s) using prior probability")
        else:
            logging.info("(e|s) No probability")
        self.threshold = threshold

    def rank(self, spot):
        eml = EntityMatchList()
        match = None
        if self.RANK_BY_PRIOR:
            for entity in spot.getSpot().getEntities():
                if spot.getEntityCommonness(entity) < self.threshold:
                    logging.debug("filter entity %s, low commonness",
                                  entity.id())
                    continue
                match = EntityMatch(entity,
                                    spot.getEntityCommonness(entity),
                                    spot)
                eml.append(match)
        return eml
    
