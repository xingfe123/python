#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
from urllib.request import urlopen
from urllib.parse import quote
import json
from db import getContain


def ltpcloud(sent):
    url = "http://ltpapi.voicecloud.cn/analysis/?"
    api_key = 't3Y4A0g3dKcLPzHvBy4dZeBPLTFVxscOtiJx2kmn'
    text = quote(sent)
    try:
        result = urlopen('{0}api_key={1}&text={2}&pattern=ner&format=json'.
                         format(url, api_key, text))
        content = result.read()
        text = json.loads(content.decode('utf-8'))
        return text[0][0]
    except Exception as err:
        print(err)


def getNers(context):
    for x in ltpcloud(context):
        if x['ne'] == 'O':
            continue
        yield x['cont'], x['id'], x['pos'], x['ne']


def getCandidate(context):
    cans = getContain(context)
    words = context.split(' ')
    if len(words) > 1:
        acronym = []
        for x in words:
            cans.append(getContain(x))
            acronym.append(x[0])
        cans.append(getContain(acronym.join('')))
    # return popualrity score, which is a function of the entity's
    # inboud and outbound link counts in KB. 
    return cans


def testMain():
    for content, id, pos, ner in getNers('我是中国人'):
        print(ner)


def testCan():
    L = getCandidate('Google')
    for x  in L:
        return getCommon(x)


    

if __name__ == '__main__':
    # testMain(getNers)
    # testCan(getCandidate)

