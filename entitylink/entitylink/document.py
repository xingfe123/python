#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL


class AricleDescription(object):
    def __init__(self, title, wikiTitle, id, summary):
        self.title = title
        self.description = "NODESC"
        self.infobox = None
        self.url = "http://en.wikipedia.org/wiki/"+wikiTitle
        self.image = "http://wikiname2image.herokuapp.com/"+wikiTitle
        self.id = wikiTitle

    def toJson(self):
        return ""

    
class AnnotateDocument(object):
    def __init__(self, text):
        self.doc = text
        self.spots = []
        self.annotateDocument = None

        
class AnnotateSpot(object):
    def __init__(self, mention, linkProbability, documentFrequency, score,
                 start, end, linkFrequency, commonness, entity,
                 entityFrequency):
        self.mention = mention
        self.start = start
        self.end = end
        self.entity = entity
        self.score = score
        self.commonness = commonness
        self.entityFrequency = entityFrequency
        self.linkFrequency = linkFrequency
        self.documentFrequency = documentFrequency

    
class Document(object):
    def getField(self, name):
        return None

    def getFilds(self):
        return None

    def addField(self):
        return None

    def removeField(self):
        return None

    def getContent(self):
        return None

    
class Field(object):
    def __init__(self, name, value):
        self.name = name
        self.value = value


class FieldDocument(Document):
    def __init__(self):
        self.fields = {}

    def getField(self, field):
        return self.fields.get(field)

    def addField(self, field):
        self.fields[field.name()] = field

    def _addField(self, name, value):
        self.fields[name] = value

    def removeField(self, name):
        self.fields.remove(name)

    def getContent(self):
        return "".join(self.fields.values())

    
class SimpleDocument(FieldDocument):
    """A SimpleDocument is a Document with only one Field"""
    fieldName = "body"
    
    def __init__(self, text):
        self.super().__init__(text)
        self.body = Field(self.fieldName, text)
        self.fields.put(self.fieldName, text)
        
        
