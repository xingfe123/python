def processAnchors():
    for anchor in wikidump.getAnchors():
        if anchor.textLen() == 1 or anchor.textNumber() or \
           anchor.lowfrequency():
            continue
        anchorIndex(anchor.source(), anchor.destination(), anchor.text())


def processPages():
    for page in wikidump.getPages():
        if page.isDisambiguation() and page.isRedirect():
            continue
        pageIndex(page.title(), page.content())


def processLinks():
    for link in wikidump.getLinks():
        linkIndex(link.source(), link.destination())
class PageE(object):
    def __init__(self, ele):
        self.title, self.redirect, self.text = processPage(ele)
        self.ele = ele

    def getAchors(self):
        if self.text:
            return None
        for title, otherName in wikiLinks(self.text):
            if otherName:
                otherName = title
            yield Anchor(txt=otherName, fm=self._title, to=title)
            
    def getText(self, tag):
        child = self.ele.find(tag)
        if child is not None:
            txt = child.text
            del child
            return txt
        return ""

    def title(self):
        return self._title

    def text(self):
        return self._text
    
    def clear(self):
        self.ele.clear()

def updateDb():
    session = Session()
    for i, x in enumerate(Title.txt2titles()):
        if i % 10000 == 0:
            session.commit()
        session.add(x)

    
def testDb():
    session = Session()
    query = session.query(Title)
    for x in query.filter(Title.title.like('%air%')):
        print(x)
        
if __name__ == '__main__':
    for file in util.dir2files('/home/luoxing/data/KB'):
        IdName.addColums(file)
    
import networkx
import numpy.linalg
import pylab
n = 100
m = 500
g = networkx.gnm_random_graph(n, m)
l = networkx.generalized_laplacian(g)
e = numpy.linalg.eigvals(l)
max(e), min(e)
pylab.hist(e, bins=10)
pylab.xlim(0, 2)
pylab.show()

import networkx as nx
import db
s = db.getSession()
def makeGraph(session):
    g = nx.DiGraph()
    for src, dest in session.query(db.Anchor.source, db.Anchor.destination):
        g.add_edge(src, dest)
    return g


def islowfrequency(anchor):
    return anchor.frequency < 2


def rel(anchor, other):
    return 0.0


def vote(anchor, anchors):
    pg = anchor.frequency()
    sum = 0
    for other in anchors:
        sum += rel(other, anchor)
    return sum/pg


def lowPrior(anchor):
    return None


def disbyClassifier():
    "use as features the socre rel, and the commonless, "
    pass


def didbyThreshold():
    "to obtain the highest commonness among them"
    pass








def main2():
    authenticate("localhost:7474", 'neo4j', 'luoxing123')
    graph = Graph('http://localhost:7474/db/data/')
    db = xapian.WritableDatabase('/home/luoxing/xapian-database1')
    number = 11  #  330000
    for docid in range(1, number+1):
        print(docid)
        doc = db.get_document(docid)
        src = doc.get_value(0).decode("utf-8")
        text = doc.get_data().decode("utf-8")
        srcN = graph.find_one("Page", property_key='title',
                              property_value=src)
        if srcN is None:
            srcN = makeNode(src)
        for con, dest in wikiLinks(text):
            destN = graph.find_one("Page", property_key='title',
                                   property_value=dest)
            if destN is None:
                destN = makeNode(dest)
            addLink(graph, srcN, destN, con)
    db.close()

from sqlalchemy import create_engine, Column, Integer, String, Sequence
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///file.db', echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    
    _id = Column(Integer, Sequence('id_seq'), primary_key=True)
    name = Column(String)
    fullname = Column(String)
    password = Column(String)
    
    def __repr__(self):
        return "<User(name='%s', fullname='%s', password='%s')>" % (
            self.name, self.fullname, self.password)
    
    def __init__(self, name, fullname, password):
        self.name = name
        self.fullname = fullname
        self.password = password


session = Session()

user = User('lx', 'Luo Xing', 'luoxing123')
session.add_all([
    User(name='wendy', fullname='Wendy Williams', password='foobar'),
    User(name='mary', fullname='Mary Contrary', password='xxg527'),
    User(name='fred', fullname='Fred Flinstone', password='blah'),
])


session.add(user)
session.commit()








def addObject(name, fullname, password):
    user = User(name, fullname, password)
    session.add(user)


    def sumPrimes(n):
    return sum([x for x in range(2, n) if isprime(x)])+i


pool = Pool(3)
print(pool.map(sumPrimes, range(100) ))
print(time.time()-start)
"        LinkTo  {text:{Con}} ]->(t)  return  r;'
        s['LinkTo0'] = 'match (f:Page), (t:Page)\
        where f.title = {Src} and t.title ={Dest} \
        create (f)-[r:LinkTo]->(t)  return  r;'
        s['RedirectTo1'] = 'match (f:Page), (t:Page)\
        where f.title = {Src} and t.title ={Dest} \
        create (f)-[r:RedirectTo  {text:{Con}} ]->(t)  return  r;'
        s['RedirectTo0'] = 'match (f:Page), (t:Page)\
        where f.title = {Src} and t.title ={Dest} \
        create (f)-[r:RedirectTo ]->(t)  return  r;'
"



def isprime(n):
    if n <2 :
        return False
    if n == 2:
        return True
    for x in range(2, int(math.ceil(math.sqrt(n)))):
        if n % x == 0:
            return False
    return True
class Pool(object):
    def __init__(self, number=None):
        if number is None:
            self.number = multiprocessing.cpu_count +1
        self.pool = multiprocessing.Pool(self.number)

    def apply(self, func, args):
        pass

    
