#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import itertools
import os
import xml.etree.ElementTree as ET


class Matrix(object):
    def __init__(self, rows, cols):
        self.m = {}
        self.rows = rows
        self.cols = cols
        
    def __add__(self, other):
        self.m.update(other.m)

    def __mul__(self, other):
        assert self.cols == other.rows
        
        m = Matrix(self.rows, other.cols)
        for i in range(self.rows):
            for j in range(self.cols):
                v = 0
                for x in range(self.cols):
                    v += self.get(i, x) * self.get(x, j)
                m.set(i, i, v)

        return m
    
    def __getitem__(self, i, j):
        assert i < self.rows
        assert j < self.cols
        return self.m[str(i)+'/'+str(j)]

    def __setitem__(self, i, j, k):
        assert i < self.rows
        assert j < self.cols
        self.m[str(i)+'/'+str(j)] = k


def lazyprop(fn):
    pass


class Cycle(object):
    def __init__(self, number=1):
        self.lst = [0]*number
        self.now = 0
        self.number = number
        
    def append(self, x):
        self.now = (self.now + 1) % self.number
        self.lst[self.now] = x

    def __iter__(self):
        for i in range(self.now+1, self.number+self.now+1):
            yield self.lst[i % (self.number)]


def totalNumber(it):
    return sum(1 for x in it)


def takeN(number, lst):
    return itertools.islice(lst, number)


def takeOne(lst):
    x = head(lst,number=1)
    return x


def head(it, number=10):
    return takeN(number, it)


def tail(it, number=10):
    t = Cycle(number)
    for x in it:
        t.append(x)
    return t
    

def xml2element(path):
    dump = ET.parse(path)
    root = dump.getroot()
    for element in root:
        yield element


def dir2files(dir):
    return (os.path.join(dir, file) for file in os.listdir(dir))


def match(str, other):
    if str in other:
        return True
    elif other in str:
        return True
    return False


def printIter(lst):
    for x in lst:
        print(x)


class EntityError(Exception):
    def __init__(self, mesg):
        self.super().__init__()
        self.mesg = mesg

    def __str__(self):
        return repr(self.mesg)


def isThousands(i):
    return i % 1000 == 0
