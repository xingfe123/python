#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
from py2neo import Graph, Path, Node, Relationship, authenticate
__author__ = """luo xing xingfe123@gmail.com"""
__credits__ = """"""
from py2neo.packages.httpstream import http
http.socket_timeout = 9999
import networkx as nx


class Matrix(object):
    def __init__(self, rows, cols):
        self.m = {}
        self.rows = rows
        self.cols = cols
        
    def __add__(self, other):
        self.m.update(other.m)

    def __mul__(self, other):
        assert self.cols == other.rows
        
        m = Matrix(self.rows, other.cols)
        for i in range(self.rows):
            for j in range(self.cols):
                v = 0
                for x in range(self.cols):
                    v += self.get(i, x) * self.get(x, j)
                m.set(i, i, v)

        return m
    
    def __getitem__(self, i, j):
        return self.m[str(i)+'/'+str(j)]

    def __setitem__(self, i, j, k):
        self.m[str(i)+'/'+str(j)] = k

    

class HyperGraph(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.graph = nx.DiGraph()

    def __iter__(self):
        for x in self.graph:
            if x['type'] is 'n':
                yield x
                
    def __getitem__(self, i, j):
        return 0.0
    
    def __setitem__(self, i, j, c):
        return 0
    
    def remove_node(self, n):
        self.graph.remove_node(n)

    def remove_hyperedge(self, n):
        self.graph.remove_node(n)

    def add_node(self, n):
        self.graph.add_node(n, type='n')

    def has_node(self, n):
        self.graph.has_node(n)

    def add_hyperedge(self, nodes):
        h = nodes.join("/")
        self.graph.add_node(h, type='h')
        for x in nodes:
            if not self.graph.has_node(x):
                self.add_node(x)
        
    def clear(self):
        self.graph.clear()

    def number_of_edges(self, o, n):
        nl = self.graph.neighbors(n)
        ol = self.graph.neighbors(o)
        number = 0
        for x in nl:
            if x in ol:
                number += 1
        return number

    def number_of_nodes(self):
        return self.graph.number_of_nodes() - self.number_of_edges()

    def has_edge(self, n, o):
        nl = self.graph.neighbors(n)
        ol = self.graph.neighbors(o)
        for x in nl:
            if x in ol:
                return True
        return False
    
    def size(self):
        return sum(1 for n in self.graph.nodes() if n['type'] is 'h')


def connectToGraph(host='localhost'):
    uri = "{}:7474".format(host)
    authenticate(uri, 'neo4j', 'luoxing123')
    graph = Graph('http://'+uri+'/db/data/')
    return graph


class NeoGraph(object):
    queryOut = "match (n1:Page {{title:{}}})-[:LinkTo]->(n2:Page)\
        return count(distinct n2);"
    queryIn = "match (n1:Page )-[:LinkTo]->(n2:Page {{title:{}}})\
        return count(distinct n1);"
    queryInterIn = "match (n1:Page )-[:LinkTo]->(n2:Page {{title:{}}}) \
        ,\
        (n1:Page) -[:LinkTo]-> (n3:Page {{title:{}}})\
        return count(distinct n1);"
    
    queryUnionIn = "match (n1:Page )-[:LinkTo]->(n2:Page {{title:{}}}) \
        return count(distinct n1) union match\
        (n1:Page) -[:LinkTo]-> (n3:Page {{title:{}}})\
        return count(distinct n1);"
    queryInterIn = " match (n1:Page )-[:LinkTo]->(n2:Page {{id:{}}}) \
        ,\
        (n1:Page) -[:LinkTo]-> (n3:Page {{id:{}}})\
        return count(distinct n1);"
    queryUnionI = "match (n1:Page )-[:LinkTo]->(n2:Page {{id:{}}}) \
        return count(distinct n1) union match\
        (n1:Page) -[:LinkTo]-> (n3:Page {{id:{}}})\
        return count(distinct n1);"
    queryCon = 'match (f:Page), (t:Page)\
        where f.title = "{src}" and t.title ="{dest}" \
        create (f)-[r:{cls} {{text: "{con}"  }} ]->(t)  return  r;'
    query = 'match (f:Page), (t:Page)\
        where f.title = "{src}" and t.title ="{dest}" \
        create (f)-[r:{cls} ]->(t)  return  r;'
    queryNode = 'create (n:Page {{title :"{title}"}}) ;'
    nodeStr = ' (n:Page {{title: "{title}"}}) '

    def __init__(self, graph):
        self.graph = graph
        self.cypher = self.graph.cypher

    @property
    def order(self):
        return self.graph.order
    
    @property
    def size(self):
        return self.graph.size
    
    def compile(self, text):
        self.graph.cypher.execute(text)
        
    def addNodeQ(self, title, i):
        srcN = Node("Page", title=title, id=i)
        self.graph.create(srcN)

    def addLink(self, srcN, destN, con):
        self.graph.create(Relationship(srcN, "LinkTo", destN, text=con))
    
    def addRedirect(self, srcN, destN):
        self.graph.create(Relationship(srcN, "RedirectTo", destN))

    def addRelation(self, cls, src, dest, text=None):
        if text is None:
            r = self.graph.cypher.execute(
                NeoGraph.query.format(src=src, dest=dest, cls=cls))
        else:
            r = self.graph.cypher.execute(
                NeoGraph.queryCon.format(src=src, dest=dest,
                                         cls=cls, con=text))
        return r
    
    def degreeI(self, i):
        l = self.compile(" match (n:Page {{title:{id}}) -> (o) \
        return count(distinct(o))".format("'"+i+"'"))
        if l is None:
            return 0
        return l[0]

    def getNodeI(self, i):
        l = self.compile("match (n:Page {{id: {}}}) return n"
                         .format("'"+i+"'"))
        if len(l) > 0:
            return l[0]
        return None

    def existsNodeI(self, i):
        l = self.getNodeI(i)
        return len(l) > 0
        
    def out_degreeT(self, title):
        l = self.compile(NeoGraph.queryOut.format('"'+title+'"'))
        return l[0][1]

    def in_degreeT(self, title):
        l = self.compile(NeoGraph.queryIn.format('"'+title+'"'))
        return l[0][1]

    def inter_in_degreeI(self, i, o):
        l = self.compile(NeoGraph.queryInterI.format(
            '"'+i+'"', '"'+o+'"'))
        return l[0][1]
    
    def union_in_degreeI(self, i, o):
        l = self.compile(NeoGraph.queryUnionI.format(
            '"'+i+'"', '"'+o+'"'))
        return l[0][1]
    
    def inter_in_degreeT(self, title, other):
        l = self.compile(NeoGraph.queryInterIn.format(
            '"'+title+'"', '"'+other+'"'))
        return l[0][1]

    def union_in_degreeT(self, title, other):
        l = self.compile(NeoGraph.queryUnionIn.format(
            '"'+title+'"', '"'+other+'"'))
        return l[0][0]


def titleEncode(title):
    title = l.sub(r'\"', title)
    title = ll.sub(r"\'", title)
    # title = re.sub(r'\\', r'\\\\', title)
    return title


