#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
import logging


class SentenceSegmenter(object):
    pass


class TokenSegmenter(object):
    pass


class PosToken(object):
    def __init__(self, token, pos):
        self.toke = token
        self.pos = pos
        
    def isVerb(self):
        return self.pos.startsWith("VB")
    
    def isAdverb(self):
        return self.pos.startsWith("RB")
    
    def isConjunction(self):
        return self.pos.startsWith("CC")
    
    def isAbjective(self):
        return self.pos.startsWith("JJ")

    
class Token(object):
    def __init__(self, text, start, end):
        self.start = start
        self.end = end
        self.text = text

    def empty(self):
        return not self.text

    
class Sentence(Token):
    pass


class PosTagger(object):
    instance = None
    
    def __init__(self, tag):
        self.tag = tag

    @staticmethod
    def getInstance():
        if PosTagger.instance is None:
            PosTagger.instance = PosTagger()
           
    def tag(self, text):
        tokens = []
        if text is None:
            logging.warn("text is null")
            return tokens
        if not text:
            logging.warn("text is empty")    
            return tokens
        return tokens

class TokenSegmenter(object):
    pass

    
class Text(object):
    stopEnglish = {"about", "above", "above", "across",
                   "after", "afterwards", "again", "against", "all", "almost",
                   "alone", "along", "already", "also", "although", "always",
                   "am", "among", "amongst", "amoungst", "amount", "an",
                   "and", "another", "any", "anyhow", "anyone", "anything",
                   "anyway", "anywhere", "are", "around", "as", "at", "back",
                   "be", "became", "because", "become", "becomes", "becoming",
                   "been", "before", "beforehand", "behind", "being", "below",
                   "beside", "besides", "between", "beyond", "bill", "both",
                   "bottom", "but", "by", "call", "can", "cannot", "cant",
                   "co", "con", "could", "couldnt", "cry", "de", "describe",
                   "detail", "do", "done", "down", "due", "during", "each",
                   "eg", "eight", "either", "eleven", "else", "elsewhere",
                   "empty", "enough", "etc", "even", "ever", "every",
                   "everyone", "everything", "everywhere", "except", "few",
                   "fifteen", "fify", "fill", "find", "fire", "first", "five",
                   "for", "former", "formerly", "forty", "found", "four",
                   "from", "front", "full", "further", "get", "give", "go",
                   "had", "has", "hasnt", "have", "he", "hence", "her",
                   "here", "hereafter", "hereby", "herein", "hereupon",
                   "hers", "herself", "him", "himself", "his", "how",
                   "however", "hundred", "ie", "if", "in", "inc", "indeed",
                   "interest", "into", "is", "it", "its", "itself", "keep",
                   "last", "latter", "latterly", "least", "less", "ltd",
                   "made", "many", "may", "me", "meanwhile", "might", "mill",
                   "mine", "more", "moreover", "most", "mostly", "move",
                   "much", "must", "my", "myself", "name", "namely",
                   "neither", "never", "nevertheless", "next", "nine", "no",
                   "nobody", "none", "noone", "nor", "not", "nothing", "now",
                   "nowhere", "of", "off", "often", "on", "once", "one",
                   "only", "onto", "or", "other", "others", "otherwise",
                   "our", "ours", "ourselves", "out", "over", "own", "part",
                   "per", "perhaps", "please", "put", "rather", "re", "same",
                   "see", "seem", "seemed", "seeming", "seems", "serious",
                   "several", "she", "should", "show", "side", "since",
                   "sincere", "six", "sixty", "so", "some", "somehow",
                   "someone", "something", "sometime", "sometimes",
                   "somewhere", "still", "such", "system", "take", "ten",
                   "than", "that", "the", "their", "them", "themselves",
                   "then", "thence", "there", "thereafter", "thereby",
                   "therefore", "therein", "thereupon", "these", "they",
                   "thickv", "thin", "third", "this", "those", "though",
                   "three", "through", "throughout", "thru", "thus", "to",
                   "together", "too", "top", "toward", "towards", "twelve",
                   "twenty", "two", "un", "under", "until", "up", "upon",
                   "us", "very", "via", "was", "we", "well", "were", "what",
                   "whatever", "when", "whence", "whenever", "where",
                   "whereafter", "whereas", "whereby", "wherein", "whereupon",
                   "wherever", "whether", "which", "while", "whither", "who",
                   "whoever", "whole", "whom", "whose", "why", "will", "with",
                   "within", "without", "would", "yet", "you", "your",
                   "yours", "yourself", "yourselves", "the" }

    def __init__(self, text):
        self.text = text

    def disableStopWords(self):
        self.useStopWords = False
        return self

    def getTerms():
        pass

    @staticmethod
    def getIntersection(l, r):
        lt = Text(l)
        rt = Text(r)
        intersection = {}
        for term in lt.getTerms():
            if rt.getTermsSet().contains(term):
                intersection.add(term)
        return intersection

    @staticmethod
    def sharedTerms(l,r):
        return getIntersection(l,r).size()

    def getTermsSet():
        termSet ={}
        return termSet
    
        


    
class StandardTagger(Tagger):
    stopWatch = StopWatch()
    
    def __init__(self, name, spotter, disambiguation):
        self.name = name
        self.spotter = spotter
        self.disambiguation = disambiguation
        StandardTagger.stopWatch = StopWatch()
        
