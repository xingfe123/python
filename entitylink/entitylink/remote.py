# import execnet
import os 

def multiplier(channel, factor):
    while not channel.isclosed():
        param = channel.receive()
        channel.send(param * factor)


# gw = execnet.makegateway()
# channel = gw.remote_exec(multiplier, factor=10)
# for x in range(5):
#     channel.send(x)
#     result = channel.receive()
#     print(result)
# gw.exit()

if __name__ == '__channelexec__':
    channel.send("ok")
    channel.send(os.getcwd())
