#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
from setuptools import setup


setup(name='entityLink',
      version='0.1',
      url='http://github.com/luoxing91/EL',
      license='GPL',
      packages=['entityLink'],
      author='luo xing',
      author_email="xingfe123@gmail.com",
      install_requires=['nltk', ],
      zip_safe=False)

