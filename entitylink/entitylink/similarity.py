#!/usr/bin/env python3
# Copyright (C) 2015 by luo xing
# License: GPL

import math
""" cosine similarity between the WIkipedia article corresponding to
entity and a local context windows of 50 words around The
candidate-can edges are weighted by using

candidate have a restart probability 0
inverse document frequery score """


def sdnorm(z):
    """
    Standard normal probability density function
    """
    return math.exp(-z * z / 2.0) / math.sqrt(2 * math.pi)


# The most straightforward method is to compare the context of the
# mention and the definition of candidate entitis


class relation(object):
    def __init__(self):
        pass

    def similar(self, context, other):
        return 0.0


class dotProduct(relation):
    def similar(self, context, other):
        return context.dotProduct(other)


class JCosine(relation):
    """This function considers all terms found in the candidate entity KB
entry title,"""

    def similar(self, context, other):
        return math.cosine(context, other)

    def KL_divergence():
        pass

    def jaccardDistance():
        pass


""" between the sentence containing the mention, and the textual
description of candidate NE (first section) Entity popularity
importance of the candidate entity

"""


class Collective(object):
    """
    local method based on only similarity sim(d,e) of context d and
    entity e are hard to beat.

    """
    pass


def contain(str, other):
    return str in other or other in str


def Acronyms(object):
    def __init__(self):
        self.alias = []

    def getLong(self, name):
        return self.alias[name]


def getKB():
    pass


def getCandidate(query):
    for x in getKB():
        if nameComparison(x, query):
            yield x


def main():
    candidate = getCandidate(query)
    entity = select(candidate)
    if isNil(entity):
        return None

    numbers = 1000
    alpha = 1
    innov = uniform(-alpha, alpha, numbers)

    x = 0.
    vec = []
    for i in range(1, numbers):
        candidate = x + innov[i]
        acceptable = min(1, sdnorm(candidate) / sdnorm(x))
        u = uniform(0, 1)
        if u < acceptable:
            x = candidate
            vec.append(x)


def ref(page, other):
    """check if either document has a link to the other"""
    pass


def jprob(page, other):
    """S the set of sentences containing any entity reference"""
    pass


def rank(graphs):
    """the disambiguation phase ranks all nodes in the solution graph and
    select the best from the candidate list for each NE textual mention

    """
    graph.rank()
    Rs = Icon(can) + PageRank(can)


def selection(Rs, Rm):
    Rs.sort()
    Rm.sort()
    diff1 = Rs[0] - Rs[1]
    diff2 = Rm[0] - Rm[1]
    if diff1 > diff2:
        return Rs.selection()
    else:
        return Rm.selection()
