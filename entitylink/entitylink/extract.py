#!/usr/bin/env python3
import re
import xml.etree.cElementTree as ET
#  from . import db
import csv

from progressbar import ProgressBar
# import xapian


# import lxml.etree as ET


def idNameGenerator(namefile):
    with open(namefile, 'r') as infile:
        reader = csv.reader(infile, delimiter=':')
        for row in reader:
            yield row[1], row[2]


def pipeIds(namefile):
    with open(namefile, 'r') as infile:
        reader = csv.reader(infile, delimiter=':')
        for row in reader:
            yield row[1]


ns = {'xmlns': "http://www.mediawiki.org/xml/export-0.10/"}
rediect = re.compile(r'#REDIRECT \[\[([^[]*?)\]\]')


def Tag(txt, ns=ns):
    return '{'+ns['xmlns']+'}'+txt


def isTitleE(child):
    return child.tag == Tag('title')


def isIdE(child):
    return child.tag == Tag('id')


def isTextE(child):
    return child.tag == Tag('text')


def isRedirectE(child):
    return child.tag == Tag('redirect')


def isPageE(elem):
    return elem.tag == Tag('page')


def isRevisionE(child):
    return child.tag == Tag('revision')


def getRedirectE(child):
    return child.attrib['title']


def getTextE(child):
    return child.find(Tag('text')).text


def getTitleE(elem):
    return elem.find(Tag('title')).text


def processPage(elem):
    redirect = None
    text = None
    for child in elem.iter():
        if isTitleE(child):
            title = child.text
        elif isRedirectE(child):
            redirect = getRedirectE(child)
            break
        elif isRevisionE(child):
            text = getTextE(child)
            break
    return title, redirect, text


def wikiLinks(text):
    link = re.compile(r'\[\[([^[]*?)\]')
    for x in link.finditer(text):
        linktext = x.group(1).split('|')
        if len(linktext) > 1:
            title, text = linktext[0], linktext[1]
        else:
            title = linktext[0]
            text = None
        title = title.strip()
        if text:
            text.strip()
        yield title, text


def extractLinks(elem):
    text = None
    for child in elem.iter():
        if isTitleE(child):
            src = child.text
            if len(src) == 1 or src.isnumeric():
                return 
        elif isRedirectE(child):
            dest = getRedirectE(child)
            yield text, src, dest
        elif isRevisionE(child):
            for dest, text in wikiLinks(getTextE(child)):
                yield text, src, dest
            break


def isRedirectT(text):
    if text is None:
        return True
    m = rediect.match(text)
    if m:
        return True
    return None


def RedirectT(text):
    if text is None:
        return None
    m = rediect.match(text)
    if m:
        return m.group(1)
    return None


def getPages(path):
    infile = open(path, 'rb')
    context = ET.iterparse(infile, events=('end',))
    text = None
    title = None
    for event, elem in context:
        if isPageE(elem):
            yield title, text
            elem.clear()
        elif isTitleE(elem):
            title = elem.text
        elif isTextE(elem):
            text = elem.text



def getNormalPage(path):
    infile = open(path, 'rb')
    context = ET.iterparse(infile, events=('end',))
    text = None
    Id = None
    for event, elem in context:
        if isPageE(elem):
            Id = elem.find(Tag('id')).text
            revision = elem.find(Tag('revision'))
            text = revision.find(Tag('text')).text
            if isRedirectT(text):
                elem.clear()
                continue
            yield Id, text
            elem.clear()


def getTitleId(path):
    infile = open(path, 'rb')
    context = ET.iterparse(infile, events=('end',))
    Id = None
    for event, elem in context:
        if isPageE(elem):
            Id = elem.find(Tag('id')).text
            title = elem.find(Tag('title')).text
            yield Id, title
            elem.clear()


def getTextId(path):
    infile = open(path, 'rb')
    context = ET.iterparse(infile, events=('end',))
    Id = None
    for event, elem in context:
        if isPageE(elem):
            Id = elem.find(Tag('id')).text
            revision = elem.find(Tag('revision'))
            text = revision.find(Tag('text')).text
            yield Id, text
            elem.clear()



def makeCvsReader(infile):
    reader = csv.reader(infile, delimiter=',', quotechar='|')
    return reader


def makeCvsWriter(outfile):
    writer = csv.writer(outfile, delimiter=",", quotechar="|")
    return writer


def cleanLink(cvsfile, outcvsfile):
    with open(cvsfile) as infile:
        with open(outcvsfile, 'w') as outfile:
            reader = makeCvsReader(infile)
            writer = makeCvsWriter(outfile)
            prev = next(reader)
            writer.writerow(prev)
            for now in reader:
                if now[2] != prev[2] or now[1] != prev[1]:
                    prev = now
                    writer.writerow(prev)


queryCon = 'match (f:Page), (t:Page)\
        where f.title = "{src}" and t.title ="{dest}" \
        create (f)-[r:{cls} {{text: "{con}"  }} ]->(t)  return  r;'
query = 'match (f:Page), (t:Page)\
        where f.title = "{src}" and t.title ="{dest}" \
        create (f)-[r:{cls} ]->(t)  return  r;'
queryNode = 'create (n:Page {{title :"{title}"}}) ;'
nodeStr = ' (n:Page {{title: "{title}"}}) '
l = re.compile(r'"')
ll = re.compile(r"'")


def cvs2graphNode(graph, titlefile):
    i = 0
    pbar = ProgressBar(maxval=14496581)
    with open(titlefile) as infile:
        for title in infile:
            i += 1
            if not title:
                continue
            try:
                graph.create(makeNode(title))
            except Exception as e:
                print(e, i)
            pbar.update(i)


def cvs2graphRelationShip(graph, cvsfile):
    with open(cvsfile) as infile:
        reader = makeCvsReader(infile)
        next(reader)
        for link in reader:
            if len(link) > 3:
                graph.cypher.execute(queryCon.format(src=link[1],
                                                     dest=link[2],
                                                     cls=link[0],
                                                     con=link[3]))
            else:
                graph.cypher.execute(query.format(src=link[1],
                                                  dest=link[2],
                                                  cls=link[0]))

def extractAnchors(xmlfile, cvsfile, start=0, end=0):
    count = 0
    with open(cvsfile, 'w') as outfile:
        writer = csv.writer(outfile, delimiter=',', quotechar="|")
        writer.writerow(('type', 'src', 'dest', 'text'))
        for src, text in getPages(xmlfile):
            count += 1
            print(count)
            
            if text is None or src is None:
                continue
            
            dest = RedirectT(text)
            if dest:
                writer.writerow(("RedirectTo", src, dest))
                continue
            
            for dest, text in wikiLinks(text):
                if text:
                    writer.writerow(["LinkTo", src, dest, text])
                else:
                    writer.writerow(["LinkTo", src, dest])



if __name__ == '__main__':
    # main()
    # main('/home/luoxing/windows/enwiki.xml', '192.168.0.100')
    extractAnchors('/mnt/windows/zhwiki.xml', '/mnt/windows/link1.cvs')
    # graph = connectToGraph()
    # cvs2graphNode(graph, "/home/luoxing/data/title.all")
