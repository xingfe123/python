#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
from sqlalchemy import create_engine, Column, Integer, String, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from progressbar import ProgressBar
import os
import redis
from entitylink import utils

import logging
logger = logging.getLogger()

class Store(object):
    def fetch(self, oid):
        raise NotImplementedError

    def save(self, oid):
        raise NotImplementedError

    def delete(self, oid):
        raise NotImplementedError

    def flush(self):
        raise NotImplementedError

    def fetch_all(self):
        raise NotImplementedError

    def iter_ids(self):
        raise NotImplementedError

    def fetch_iter(self, oiter):
        for oid in oiter:
            yield self.get(oid)

    def save_iter(self, size):
        return BatchInserter(self, size)

    @staticmethod
    def Get(store_id, url = None):
        if url.startswith('redis'):
            return RedisStore(store_id, url=url)
        
class BatchInserter(object):
    def __init__(self, store, size):
        self.store = store
        self.size = size
        self.batch = []

    def flush(self):
        if self.batch:
            self.store.save_iter(self.batch)
            self.batch = []

    def save(self, obj):
        self.batch.append(obj)
        if len(self.batch) > self.size:
            self.flush()

    def __enter__(self):
        self.batch = []
        return self

    def __exit__(self, t, value, traceback):
        self.flush()

import re
import json
class RedisStore(Store):
    def __init__(self, namespace, url='redis://localhost' ):
        self.kvs = redis.from_url(url)
        self.ns = namespace

    def _key(self, oid):
        return self.ns+':'+oid
    
    def _oid(self, key):
        return key[len(self.ns)+1:].decode('utf-8')

    def fetch(self, oid):
        data = self.kvs.get(self._key(oid))
        return self.deserialise(data) if data is not None else None

    def iter_ids(self):
        for key in self.keys():
            yield self._oid(key)

    def save(self, obj):
        self.kvs.set(self._key(obj['_id']), self.serialise(obj))

    def save_iter(self, oiter):
        self.kvs.mset({
            self._key(obj['_id']) :self.serialise(obj) for obj in oiter
        })
            
    def flush(self):
        keys = self.keys()
        if keys:
            self.kvs.delete(keys)

    def keys(self):
        return self.kvs.keys(re.escape(self._key(''))+ '*')

    def deserialise(self, data):
        return json.loads(data)

    def serialise(self, obj):
        return json.dumps(obj)
    
    


engine = create_engine('sqlite:///file.db', echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()
session = Session(bind=engine)


class TableCreator(object):
    def __init__(self, session):
        self.session = session

    def addObject(self, obj):
        self.session.add(obj)

    def index(self, source, dest, text):
        raise NotImplementedError("no index")

    def close(self):
        self.session.commit()

    def addObjects(self, lines):
        for line in lines:
            self.session.add(line)


class IdName(Base):
    __tablename__ = 'idName'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    type = Column(Enum('PER', 'UKN', 'GPE', 'ORG'), nullable=False)

    def __repr__(self):
        return "<IdName(id='%s', name='%s', type='%s')>" %\
            (self.id, self.name, self.type)
    
    @staticmethod
    def createTable():
        IdName.__table__.create(engine)
    
    @staticmethod
    def make(entity):
        name, type, id = utils.idMapName(entity)
        return IdName(id=id, name=name, type=type)

    @staticmethod
    def addColums(path):
        session = Session()
        for entity in utils.xml2element(path):
            session.add(IdName.make(entity))
        session.commit()


class Word(Base):
    __tablename__ = 'words'
    txt = Column(String, primary_key=True, nullable=False)
    number = Column(Integer)

    def __repr__(self):
        return "<Word(txt='%s', number='%s')>" %\
            (self.txt, self.number)
    
    @staticmethod
    def tuple2Word(tu):
        key, value = tu
        return Word(txt=key, number=value)

    @staticmethod
    def createTable():
        Word.__table__.create(engine)


class Anchor(Base):
    __tablename__ = 'Anchors'
    source = Column(String, nullable=False)
    destination = Column(String, nullable=False)
    text = Column(String)
    id = Column(Integer, primary_key=True)

    def __repr__(self):
        return "<Anchor(src='%s', dest='%s', text='%s', ')>" %\
            (self.source, self.destination, self.text)

    def __init__(self, source, dest, text):
        self.text = text
        self.source = source
        self.destination = dest

    def getText(self):
        return self.text

    @staticmethod
    def createTable():
        Anchor.__table__.create(engine)
        

class Link(Base):
    __tablename__ = 'links'
    fm = Column(Integer, primary_key=True, nullable=False)
    to = Column(Integer, primary_key=True, nullable=False)
    
    def __repr__(self):
        return "<Node(fm='%s', to='%s')>" %\
            (self.fm, self.to)
    
    @staticmethod
    def createTable():
        Link.__table__.create(engine)


class Title(Base):
    __tablename__ = 'title'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    namespace = Column(Integer)

    def __repr__(self):
        return "<Title(title='%s', id='%s', namespace='%s)>" %\
            (self.title, self.id, self.namespace)

    @staticmethod
    def createTable():
        Title.__table__.create(engine)
    
    @staticmethod
    def bytes2Title(str):
        namespace, id, title = str.split(':', 2)
        title = title.rstrip('\n')
        id = int(id)
        namespace = int(namespace)
        return Title(id=id, namespace=namespace, title=title)
    
    @staticmethod
    def txt2titles():
        file = "enwiki-20150304-pages-articles-multistream-index.txt"
        repos = "/home/luoxing/"
        path = repos+file
        with open(path, mode='r') as index:
            for line in index:
                yield Title.bytes2Title(line)


def getContain(context, session=session):
    return session.query(Title).filter(Title.title.like('%'+context+'%')).all()


redisNames = redis.Redis(host='localhost', port=6379, db=1)
redisIds = redis.Redis(host='localhost', port=6379, db=0)
dumpId = redis.Redis(host='localhost', port=6379, db=3)
# dumpText = redis.Redis(host='localhost', port=6379, db=5)
from  pymongo import MongoClient
db = MongoClient('localhost',27017).wiki
dumpText = db.posts

def mongo_set(dumpText, id, content):
    post = {"id": id, "context": content }
    dumpText.insert_one(post)

    
def copyfile2redids(names):
    pbar = ProgressBar(maxval=getTitleNumber())
    number = 0
    for i, (ID, name) in enumerate(names):
        if redisNames.get(name.encode('utf-8')):
            number += 1
        else:
            print(name.encode('utf-8'))
        pbar.update(i+1)


def makeNameDict(idNames):
    pbar = ProgressBar(maxval=getTitleNumber())
    for number, (titleID, name) in enumerate(idNames):
        pbar.update(number+1)
        redisNames.set(name, titleID)
    print(redisNames.dbsize())


def rmapIdNames(src=None):
    src = redis.Redis(host='localhost', port=6379, db=1)
    dest = redis.Redis(host='localhost', port=6379, db=0)
    pbar = ProgressBar(maval=src.dbsize())
    number  = 0
    for key in db.keys():
        number += 1
        pbar.update(number)
        dest.set(id,name)

def TotalNumber():
    return 5347649


def redisNamesOk():
    return getTitleNumber() == getTotalPage()


def getTotalNumber():
    # return 15347649
    return int(os.popen('wc -l /home/luoxing/data/all.txt').read().split()[0])
    


if __name__ == '__main__':
    # os.chdir('/home/luoxing/data/')
    #  makeIdName('enwiki-articles.txt', 'id2name.txt', 'name2id.txt')
    makeNameDict(idNameGenerator('/home/luoxing/data/all.txt'))


