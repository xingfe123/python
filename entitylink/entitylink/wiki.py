#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
import urllib.parse
# from entity import EntityType
import xapian


def extractDocument(element):
    text = element.findtext('wiki_text')
    id = element.get('id')
    type = element.get('type')
    doc = xapian.Document()
    doc.add_value(1, id)
    doc.add_value(2, type)
    doc.set_data(text)
    return doc


class Wiki(object):
    def __init__(self, domain, script, throttle):
        if (domain is None) or (not domain):
            self.domain = "en.wikipedia.org"
        self.domain = domain
        self.script = script
        self.throttle = throttle

    def initVars(self):
        temp = "http://"+self.domain+self.script
        if self.maxlag >= 0:
            self.apiUrl = temp + "/api.php?maxlag=" + self.maxlag +\
                "&format=xml&"
            self.base = temp + "/index.php?maxlag=" + self.maxlag + "&title="
        else:
            self.apiUrl = temp + "/api.php?format=xml&"
            self.base = temp + "/index.php?title="
        self.query = self.apiUrl + "action=query&"
        if self.redirect:
            self.query += "redirects&"

    def login(self, username, password):
        username = self.normalize(username)
        buffer = "lgname="
        buffer += urllib.parse.urlencode(username)

    @staticmethod
    def normalize(username):
        return ""
