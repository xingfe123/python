#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import Random
import math


class LinkClassifer(object):
    def __init__(self, trainData):
        self.trainData = trainData
        self.trainer = BreezeLogisticRegressionTrainer()
        self.classifier = self.trainer.train(trainData)
        
    def score(self, query):
        return self.classifier(query)


def findThreshold(data):
    randomized = Random.shuffle(data)
    split = math.ceil(len(randomized)/10.0)
    trainData = randomized.drop(split)
    testData = randomized.take(split)
    classifier = LinkClassifer(trainData)
    scores = [(f.label, classifier.score(f.item)) for f in testData]
    rankedAnswer = scores.sortBy(f._2).reverse

    total = 0
    correct = 0
    totalCorrect = len([x for x in rankedAnswer if x._1])
    thresholds = []
    for r in rankedAnswer:
        total += 1
        if r._1:
            correct += 1
        recall = correct/totalCorrect
        precision = correct/total
        fscore = 2*recall*precision/(recall + precision)
        thresholds.append((r._2, fscore))
    thresholds.sortBy(fscore)
    
