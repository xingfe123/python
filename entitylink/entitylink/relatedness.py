#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
from math import log


class Relatedness(object):
    def set(self, x, y):
        self.x = x
        self.y = y
        k = Couple(x, y)
        if self.cache_enabled and self.cache.containsKey(k):
            self.score = self.cache.get(k)
            return
        self.score = self.score()
        if self.cache_enabled:
            self.cache.put(k, self.score)

            
class MilneRelatedness(Relatedness):
    inf = []
    W = inf.size()
    logW = log(W)
    
    def __init__(self, x, y):
        self.super().__init__(x, y)
        self.inX = []
        self.inY = []
        
    def score(self):
        inX = self.inf.getNeighbours(self.x)
        inY = self.inf.getNeighbours(self.y)
        xs = inX.size()
        ys = inY.size()
        maxXY = max(xs, ys)
        minXY = min(xs, ys)
        if minXY == 0:
            return 0
        intersection = self.intersection(inX, inY)
        if intersection == 0:
            return 0
        rel = 1-(log(maxXY)-log(intersection))/(self.logW-log(minXY))
        if rel < 0:
            rel = 0
        return rel

    @staticmethod
    def intersection(inX, inY):
        i = 0
        j = 0
        size = 0
        xs = len(inX)
        ys = len(inY)
        while i < xs and j < ys:
            if inX[i] < inY[j]:
                i += 1
                continue
            if inX[i] > inY[j]:
                j += 1
                continue
            size += 1
            i += 1
            j += 1
        return size
    
    def copy(self):
        rel = MilneRelatedness(self.x, self.y)
        rel.setScore(self.score)
        return rel
    
    @staticmethod
    def getName():
        return "milne"
    
    @staticmethod
    def hasNegativeScore():
        return False
    
        
class RelatednessFactory(object):
    def __init__(self, type):
        self.type = type
        if self.relmap.containsKey(type):
            self.relmap.put(type, self.relatedness)
        else:
            self.relatedness = self.relmap.get(type)
        if self.relatedness is None:
            raise TypeError("cannot find related"+type)
        
            
class Couple(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y


def rank(s1_cans,s2):
    
    """proposition s1 s2 and for each spot c1 c2, a relatedness function
    meaure improves EL acurracy if the follow constraint holds:
    meaure(s1,s2) > meaure(s1,no-s2)
    """
    return None





































