#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import nltk
import pickle
import time


def paragraph(file):
    para = ''
    for line in sys.stdin:
        line = string.strip(line)
        if line.isspace() and not para.isspace():
            yield para
        else:
            if para != '':
                para += ' '
                para += line


class Document(object):
    def __init__(self, text, mentions):
        self.text = text
        self.mentions = mentions
    
    def __repl__(self):
        return self.text

    def entitys(self):
        return self.mentions.keys()

    def entityId(self, entity):
        return self.mentions[entity]


class GeneratorDocument(object):
    savepath = '/home/luoxing/tmp/generatorDocument'

    def __init__(self, path):
        self.docs = [x for x in generatorDocument(path)]
        self.now = -1
        self.total = len(self.docs)
        
    def dump(self):
        f = open(GeneratorDocument.savepath, "wb")
        pickle.dump(self, f, 1)
    
    def __next__(self):
        if self.now == len:
            raise StopIteration()
        self.now += 1
        return self.docs[self.now]
    
    def __iter__(self):
        return self

    @staticmethod
    def load():
        f = open(GeneratorDocument.savepath, "rb")
        return pickle.load(f)


TXT = '/home/luoxing/windows/wiki-annot30/wiki-annot30'


def generatorDocument(txtfile):
    with open(txtfile) as infile:
        while True:
            text = infile.readline()
            if not text:
                break
            line = infile.readline().strip('\n')
            lst = line.split("\t")
            l = len(lst)
            mention = {}
            for i in range(l):
                if i % 2 == 0:
                    m = lst[i]
                    t = lst[i+1]
                    mention[m] = t
            yield Document(text, mention)


def Mention(object):
    def __init__(self, name, context):
        self.name = name
        self.context = context
        self.begin = 0
        self.end = 0
        
    @staticmethod
    def generatorMentions(text):
        return None

from time import time
def timeit():
    start = time()
    for i in range(10):
        l = GeneratorDocument.load()
    print(time()-start)
    start = time()
    for i in range(10):
        d = GeneratorDocument(TXT)
    print(time()-start)
    

def named_entities(text):
    
    for sentence in nltk.sent_tokenize(text):
        for tokenized in nltk.word_tokenize(sentence):
            for tagged in nltk.pos_tag(tokenized):
                yield nltk.ne_chunk(tagged)

import redis
def testMentions():
    l = GeneratorDocument.load()
    names = redis.Redis(host='localhost', port=6379, db=0)
    number = 0
    for doc in l:
        number += 1
        if number > 4:
            break
        for entity in doc.entitys():
            print(entity)
            print(names.get(doc.entityId(entity)), )
            #assert names[entity] == doc.entityId(entity)
        print(number)
