#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
import logging
import models


class RamSpotFile(object):
    instance = None

    @staticmethod
    def getInstance(self):
        if RamSpotFile.instance is None:
            self.instance = RamSpotFile()
        return self.instance

    def getOffset(self, start, end):
        pass

    def dumpSpotFile(self, file):
        pass


class SpotRepository(object):
    def getSpot(self, spot):
        pass


class RamSpotRepository(SpotRepository):
    pass


class SpotRepositoryFactory(object):
    def getInstance(self, type):
        if type.equals("ram"):
            logging.info("Use Ram  spot Repository")
            return RamSpotRepository()


class SpotRead(object):
    def __init__(self, file, freqfile):
        pass


class SpotManager(object):
    pass


class SpotMatch(object):
    def __init__(self, spot, entities, field=None, start=0):
        self.spot = spot
        self.entities = entities
        self.field = field
        self.start = start

    def __lt__(self, other):
        return self.spot.idf < other.spot.idf

    def __gt__(self, other):
        return other.__lt__(self)

    def __eq__(self, other):
        if self == other:
            return True
        if other is None:
            return False


class SpotMatchList(list):
    def append(self, match):
        logging.debug("adding spot %s ", match.spot.mention)
        self.super().append(match)

    def normalizeSpotProbability(self):
        total = 0
        for match in self:
            total += match.probability
        for match in self:
            match.probability /= total

    def indexOf(self, spot):
        i = 0
        for sm in self:
            if sm.spot.equals(spot):
                return i
            i += 1
        return -1

    def sort(self):
        self.sort(key=lambda spotMatch: spotMatch.probability)

    def sortByStartPosition(self):
        self.sort(key=lambda spotMatch: spotMatch.start)


class Spot(object):
    def init(self, mention):
        self.mention = mention
        self.entities = set()

    def __init__(self, spot, entities, link, freq):
        self.mention = spot
        self.freq = freq
        self.link = link
        self.entities = entities
        self.updateLinkProbability()
        self.updateIDf()
        logging.debug("spot [%s] idf = %s", self.spot, self.idf)

    # @staticmethod
    # def makeSpot(spot,entities,link,freq):

    def updateLinkProbability(self):
        self.linkProbability = min(self.link/self.freq, 1.0)

    def updateIdf(self):
        self.idf = self.entity.size()/(self.freq+0.5)


def vocabulary(spot):
    """it can be easily built by considering the article Titile along with
    the anchor text of all internal wikipedia hyperlinks

    """
    return {}


def anchor(page):
    for hyperlink in page:
        yield hyperlink


def spots(doc):
    return {}


def candidates(spot):
    return {}


def linkProbability(spot):
    """the number of time spot occurs as a mention in KB / its total
    number of occurrences

    """
    return 0.0


def commonness(candidate, spot):
    """The number of spot of candidate in KB referring to candidate/ its
    total number of occurrences"""
    return 0.0

"""
threshold on minimun linking probability and commonness has been
proven to be a simple and effective strategy to limit the number of
spots and candidates without harming the recall of the entity linking
process
"""


def link(spot, candiates):
    if candiates.empty():
        return None
    if candiates.size() == 1:
        return spot, candidates.first()
    else:
        return disambiguation(spot, candiates)


def disambiguation(s, cs):
    score = 0
    best = None
    for can in cs:
        now = relatedness(s, can)
        if now > score:
            best = can
            score = now
    return s, best, score


def relatedness(a, b):
    "Wikipedia-based relatedness function proposed by Milne and Witten"
    return models.relatedness.default(a, b)
