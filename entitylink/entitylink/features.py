#! usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
from math import log
W =100

FSET = set()
class Feature(object):
    def __init__(self):
        self._tag = None

    def Extractable(c):
        FSET.add(c)
        return c

    @property
    def tag(self):
        return self._tag

    @tag.setter
    def tag(self, value):
        self._tag = value

    @property
    def id(self):
        return self.__class__.__name__ + \
        ('' if not self.tag else '[{}]'.format(self.tag))

    def __call__(self, doc):
        state = self.compute_doc_state(doc)
        for chain in doc.chains:
            for c in chain.candidates:
                c.features[self.id] = self.compute(doc, chain, c, state)
        return doc

    def compute_doc_state(self, doc):
        return None

    def compute(self, doc, chain, candidate, state):
        " return value of the feature for th candidate"
        raise NotImplementedError


import logging
import math
logger = logging.getLogger()


class LogProbabilityFeature(Feature):
    def compute(self, doc, chain, candidate, state):
        return math.log(self.probability(doc, chain, candidate, state))

    def probability(self, doc, mention, candidate, state):
        raise NotImplementedError

import features
import model
@features.Feature.Extractable
class EntityProbability(LogProbabilityFeature):
    def __init__(self, entityPrior):
        self.tag = entityPrior
        self.epm = model.EntityPrior(self.tag)

    def probability(self, doc, mention, can, state):
        return self.epm.prior(can.id)

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('entity_prior_model_atg', metavar='ENTITY')
        p.set_defaults(featurecls=cls)


@features.Feature.Extractable
class NameProbability(LogProbabilityFeature):
    pass


def probability(entity):
    return entity.in_count()/W

def entroy(entity):
    p = probability(entity)
    np = 1-p
    return p*log(p)-np*log(np)

def conditionalProbability(a,b):
    up = a.in_cup_count(b)
    return up / b.in_count()


def isLinkto(a,b):
    return a.isLinkto(b)

def linkProbability(a,b):
    p =a.out_count() 
    if p != 0:
        return 1 / p
    return 0

def friend(a,b):
    down = a.out_count()
    up = a.out_cup_in_count(b)
    return up /down

def kl_divergence(a, b):
    pa = probability(a)
    pb = probability(b)
    return pa*log(pa/pb)+ (1-pa) * log((1-pa)/(1-pb))

# Symmetric Features

def co_citation(a, b):
    return a.mw(b)

def jaccard(a, b):
    return a.in_cup_in_count(b)/a.in_cup_in_count(b)

def isLink(a, b):
    return a.isLinkTo(b) or b.isLinkTo(a)

def averageFriend(a, b):
    return 0.5 * (friend(a, b) + friend(b, a))

def outMW(a, b):
    return 0.0

def inOutMW(a, b):
    return 0

def jaccard_out(a, b):
    return a.out_cup_out_count(b)/a.out_cup_out_count(b)

def jaccard_in_out(a, b):
    return 0.0

def x_2(a, b):
    return b.in_cap_in_count(a) * (W - b.in_cup_in_count(a))
def x_out_2(a, b):
    return 0.0
def x_in_out_2(a, b):
    return 0.0

def pmi(a, b):
    return log(b.in_cap_in_count(a)) +log(W)-\
        log(a.in_cout()) -log(b.in_count())
    

class Annotation(object):
    def __init__(self, qid, inter, primaryId, mentionText, score):
        self.qid = qid
        self.id = id
        self.mention = mentionText
        self.score = score
        
    def to_csv(self):
        return '{self.qid}\t{self.id}\t{self.mention}\t{self.score}'.format(
            self=self)

    
def doc_length(db, doc):
    len = {}
    count = 0
    for term in doc.termlist():
        if term[0] != 'S':
            break
        count += term.get_wdf()
    len['title'] = count
    len['whole'] = db.get_doclength(doc.get_docid())
    len['body'] = len['whole'] - len['title']
    return len


def termfreq(doc, query):
    tf = {}
    for terms in doc.termlist():
        for word in query:
            tf[word] = terms.get_wdf()
    return tf


def inverse_doc_freq(db, query):
    totaldoc = db.get_doccount()
    idf = {}
    for word in query:
        if db.term_exists(word):
            df = db.get_termfreq(word)
            idf[word] = math.log10(totaldoc/(1+df))
        else:
            idf[word] = 0
    return idf

if __name__ == '__main__':
    pass
