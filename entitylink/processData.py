#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import extractTextId
import extractTitleId
import makeTitleIndex 

if __name__ == '__main__':
    enwiki = '/home/luoxing/windows/enwiki.xml'
    indexpath = '/home/luoxing/result/xapian-title1'
    
    extractTextId.main(enwiki)
    extractTitleId.main(enwiki)
    makeTitleIndex.main(enwiki, indexpath)
    
