#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
# clojure 

from entitylink.extract import pipeIds
from entitylink.graph import connectToGraph
from entitylink.db import TotalNumber
from py2neo import Node
from progressbar2 import ProgressBar
if __name__ == '__main__':
    graph = connectToGraph('192.168.71.1')
    # cvs2graphNode(graph, "/home/luoxing/title.all")
    pbar = ProgressBar(TotalNumber())
    for number, ID in enumerate(pipeIds('/home/luoxing/data/all.txt')):
        graph.create(Node("Page", id=ID))
        # print(title)
        pbar.update(number+1)
    print(graph.size)
