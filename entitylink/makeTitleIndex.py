#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL
from entitylink.extract import getTitleId
import xapian
import sys


def main(enwiki, dbPath):
    if dbPath is None:
        dbPath = "/home/luoxing/result/xapian-title1"
    if enwiki is None:
        enwiki = "/home/luoxing/windows/enwiki.xml"
    db = xapian.WritableDatabase(dbPath, xapian.DB_CREATE_OR_OVERWRITE)
    indexer = xapian.TermGenerator()
    indexer.set_stemmer(xapian.Stem("english"))
    for i, (ID, title) in enumerate(getTitleId(enwiki)):
        doc = xapian.Document()
        doc.add_value(0, ID)
        doc.set_data(title)
        indexer.set_document(doc)
        indexer.index_text(title)
        #  add the document to the database
        db.add_document(doc)
        sys.stdout.write(str(i+1)+'\r')
    
    
if __name__ == '__main__':
    main("/home/luoxing/windows/enwiki.xml",
         "/home/luoxing/result/xapian-title1")
