#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import sys
import os
from cheshire3.baseObjects import Session

from cheshire3.internal import cheshire3Root
from cheshire3.server import SimpleServer

if __name__ == '__main__':
    session = Session()  # 8
    servConfig = os.path.join(cheshire3Root, 'configs',
                              'serverConfig.xml')  # 9
    serv = SimpleServer(session, servConfig)  # 10

    db = serv.get_object(session, 'db_tei')  # 11
    docFac = db.get_object(session, 'defaultDocumentFactory')  # 12
    docParser = db.get_object(session, 'TeiParser')  # 13
    recStore = db.get_object(session, 'TeiRecordStore')  # 14

    docFac.load(
        session, "/home/luoxing/data/test.xml", cache=2, tagName='page')
    db.begin_indexing(session)
    recStore.begin_storing(session)
    for doc in docFac:
        try:
            rec = docParser.process_document(session, doc)
        except:
            print(doc.get_raw(session))
            sys.exit()
            id = recStore.create_record(session, rec)
            db.add_record(session, rec)
            db.index_record(session, rec)

            recStore.commit_storing(session)
            db.commit_metadata(session)
            db.commit_indexing(session)
