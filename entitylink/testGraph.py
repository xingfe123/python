#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

from entitylink.extract import connectToGraph

if __name__ == '__main__':
    graph = connectToGraph()
    print(graph.order)
    graph.delete_all()
    print(graph.order)
