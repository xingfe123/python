#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import sys
import collections
import csv
from entitylink.graph import connectToGraph
from entitylink.extract import getTitleId
from progressbar import ProgressBar

import logging
logging.basicConfig(file='/home/luoxing/tmp/makeGraph.py.log')


def pageRank(graph, damping_factor=0.85, max_iterations=100,
             min_delta=0.00001):
    nodes = graph.nodes()
    graph_size = len(nodes)
    if graph_size == 0:
        return {}
    min_value = (1-damping_factor)/graph_size
    # itialize the page rank dict with 1/N for all nodes
    
    pageRank = dict.fromkeys(nodes, 1.0)
    m = collections.defaultdict(dict)
    diff = 0.0 
    for i in range(max_iterations):
        for node in nodes:
            rank = min_value
            for ref in graph.incidents(node):
                rank += damping_factor * pageRank[ref] * m[ref][node] / \
                        sum(m[ref][x] for x in m[ref])
                diff += abs(pageRank[node] - rank)
                pageRank[node] = rank
        print('This is No.{} iteration'.format(i+1))
        print(pageRank)
        if diff < min_delta:
            break
    return pageRank


def makeAllGraphNode():
    graph = connectToGraph()
    enwiki = '/home/luoxing/windows/enwiki.xml'
    # graph.schema.create_uniqueness_constraint("Page", "id")
    tx = graph.cypher.begin()
    statement = "create  (:Page {title:{T}, id:{I} });"
    for i, (Id, Title) in enumerate(getTitleId(enwiki)):
        sys.stdout.write(str(i)+'\r')
        if i+1 <= start:
            continue
        tx.append(statement, {"I": Id, "T": Title})
        if i % 100 == 0:
            tx.commit()
            tx = graph.cypher.begin()
    tx.commit()


def makeQueryDict(t,f):
    head = 'match (f:Page), (t:Page)  where f.title = {Src} \
    and t.title = {Dest} create (f)-[r:'+t
    if f is True:
        head += " {text:{Con}}"
    head += "]->(t) return r; "
    return head
        

def makeAllGraphLinks():
    graph = connectToGraph()
    with open('/home/luoxing/data/link.cvs') as infile:
        reader = csv.reader(infile, delimiter=',', quotechar="|")
        tx = graph.cypher.begin()
        pbar = ProgressBar(maxval=278475510)
        for i, row in enumerate(reader):
            if i+1 <= 11:
                continue
            if len(row) >= 4:
                s = makeQueryDict(row[0], True)
                tx.append(s, {"Src": row[1], "Dest": row[2],
                              "Con": row[3]})
            else:
                s = makeQueryDict(row[0], False)
                tx.append(s, {"Src": row[1], "Dest": row[2]})
            if i % 100 == 0:
                tx.commit()
                tx = graph.cypher.begin()
                pbar.update(i+1)
        
        tx.commit()
        
if __name__ == '__main__':
    # makeAllGraphNode()
    makeAllGraphLinks()  # pass


