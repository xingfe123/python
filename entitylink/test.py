#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

import itertools


def compress(data):
    return ((len(list(group)), name)
            for name, group in itertools.groupby(data))


def decompress(data):
    return (char * size for size, char in data)


def main():
    pass



