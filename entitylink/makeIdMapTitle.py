#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

from entitylink.db import redisIds, getTotalNumber, idNameGenerator
from progressbar2 import ProgressBar

if __name__ =='__main__':
    path = '/home/luoxing/data/all.txt'
    pbar = ProgressBar(getTotalNumber())
    
    for i, (id, name) in enumerate(idNameGenerator(path)):
        # print(id, name)
        redisIds.set(id, name)
        pbar.update(i+1)
