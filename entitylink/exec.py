#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

from entitylink.index import TitleSearch
from entitylink import graph
dbpath = '/home/luoxing/windows/xapian-title'
titleS = TitleSearch(dbpath)


def getCan(mention, titleS):
    titleS.query_str('"'+mention+'"')
    Ids = [m.document.get_value(0) for m in titleS.get_mset(0, 20)]
    unique = set(Ids)
    return unique


def rel(x, y):
    if x is y:
        return 0.0
    _, page = x.split('/')
    _, other = x.split('/')
    return graph.sim(page, other)


def elink(context, mentions):
    words = context.split()
    local = {}
    for m in mentions:
        local[m] = words.find(mentions)

    nodes = set()
    for m in mentions:
        # print(m)
        cans = getCan(m, titleS)
        nodes.adds([m+'/'+i for i in cans])
    # print(words)
    score = {}
    for x in nodes:
        for y in nodes:
            if x is y:
                continue
            score[x] += rel(x, y)
    result = {}
    for x in score:
        m, i = x.split('/')
        if score[x] > score[m+'/'+result[m]]:
            result[m] = i
    return result


def main():
    infile = '/home/luoxing/windows/wiki-annot30/wiki-annot30'
    
    with open(infile) as data:
        context = data.readline()
        mentions = [x for i, x in
                    enumerate(data.readline().split('\t')) if i % 2 == 0]
        
        result = []
        for k, i in elink(context, mentions):
            result.append(k)
            result.append(i)

        return "\t".join(result)

if __name__ =='__main__':
    main()
