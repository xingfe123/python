#! /usr/bin/env python3
# -*-coding: utf-8 -*-
# Copyright (C) 2015 by luo xing
# License: GPL

"""
Module Docstring
Docstrings: http://www.python.org/dev/peps/pep-0257/
"""


__author__ = 'Luo Xing (xingfe123@gmail.com)'
__copyright__ = 'Copyright (c) 2015 Luo xing'
__license__ = 'New-style BSD'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'  # Versioning: http://www.python.org/dev/peps/pep-0386/
