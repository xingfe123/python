#!/usr/bin/env python3
# Copyright (C) 2015 by luo xing
# License: GPL


def pageRank(graph, damping_factor=0.85, max_iterations=100, min_deta=0.01):
    nodes = graph.nodes()
    size = len(nodes)
    if size == 0:
        return {}
    min_value = (1.0 - damping_factor) / size

    page_rank = dict.fromkeys(nodes, 1.0)
    for i in range(max_iterations):
        diff = 0
        for node in nodes:
            rank = min_value
            for refer_page in graph.incidents(node):
                rank += damping_factor * pageRank[refer_page] / len(
                    graph.neighbors(refer_page))

            diff += abs(page_rank[node] - rank)
            page_rank[node] = rank

        print("this is No.%s iteration", i + 1)
        print(page_rank)

        if diff < min_deta:
            break

    return page_rank
