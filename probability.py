#! 、usr/bin/env python3
# Copyright (C) 2015 by luo xing
# License: GPL

import numpy as np
from math import exp, log


def urnRule(Seq):
    n = 0.0
    m = 0.0
    for a, b in Seq:
        if b:
            n += 1
            if a:
                m += 1
    return m/n


def ln(x):
    return np.log(x)


def entropy(proSeq):
    """
    >>> entropy([0.5,0.5])
    0.69314718055994529
    """
    return sum([-x*ln(x) for x in proSeq])


def equal_entropy2(n):
    return log(n, 2)


def entropy2(proSeq):
    """
    >>> entropy2([0.5,0.5])
    1.0
    """
    return log(exp(1), 2)*entropy(proSeq)


def isProbability(seq):
    """
    >>> isProbability([0.1 ,0.9])
    True
    """
    return sum(seq) == 1 and all([x > 0 for x in seq])


def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / 2 * np.power(sig, 2.))


def powerFunction(i):
    """
    >>> square = powerFunction(2)
    >>> square(4)
    16
    """
    def powers(x):
        return np.power(x, i) 
    return powers


def gaussianFunction(e, v):
    def gaussian(x):
        return exp(-(x-e)**2/(2*v**2))
    return gaussian


def logistic(x):
    return 1/(1+exp(-x))


def tanh(x):
    return 2*logistic(x)-1


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    print("hh")
    l = [(1,2),(-1,1)]
    #    for mu, sig in l:
#        mp.plot(gaussian(np.linspace(-3, 3, 120), mu, sig))
#    mp.show()
