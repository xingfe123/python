#! usr/bin/env python3
# Copyright (C) 2015 by luo xing
# License: GPL

import numpy as np
import re


def check(lst, n):
    ans = 0
    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                len = lst[i] + lst[j] + lst[k]
                maxC = max(lst[i], lst[j], lst[k])
                rest = len - maxC
                if maxC < rest:
                    ans = max(ans, len)

    return ans


L = []


def slove(x, n):
    minT = 0
    for i in range(n):
        minT = max(minT, min(x[i], L - x[i]))

    maxT = 0
    for i in range(n):
        maxT = max(maxT, max(x[i], L - x[i]))


memo = []


def fib(n):
    if n <= 1:
        return n
    if memo[n] != 0:
        return memo[n]
    memo[n] = fib(n - 1) + fib(n - 2)
    return memo[n]


def x():
    t = 1

    def __init__(self):
        self.t = 1

    def y(self):
        print("jky")
        self.t = 0
        x()

    if t == 1:
        print('jl')
        y()
        t = 0


def test1():
    s = 'NP(PRP he) NP(JJS strongest)(NN rain) ADVP(RB ever) VP(VBN recorded) PP(IN in) NP(NNP India)'
    regex = r"[A-Z]{2,4}((\([A-Z]{2,4} [\w]*\)){1,2})"
    for match in re.finditer(regex, s):
        print(match.group(1))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    import string
    s = 'The quick brown fox jumped over the lazy dog.'
    print(string.capwords(s))
