#! usr/bin/env python3
# Copyright (C) 2015 by luo xing
# License: GPL

from heapq import heappush, heappop
import sys


class heap():
    def __init__(self):
        self.h = []

    def push(self, value):
        heappush(self.h, value)

    def pop(self):
        return heappop(self.h)

    def size(self):
        return len(self.h)


class stack(list):
    def push(self, item):
        self.append(item)

    def isEmpty(self):
        return not self


if __name__ == '__main__':
    if len(sys.argv) < 2:
        file = sys.stdin
    else:
        file = open(sys.argv[1])
    for line in file:
        if not line.startswith('#'):
            print(line)
