from urllib.request import urlopen
from xml.etree.ElementTree import parse


def xrml():
    u = urlopen('http://planet.python.org/rss20.xml')
    doc = parse(u)
    for item in doc.iterfind('channel/item'):
        title = item.findtext('title')
        date = item.findtext('date')
        link = item.findtext('link')
        print(title, date, link)
