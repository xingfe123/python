import json
# using requests
import urllib.request
import urllib.parse

if __name__ == '__main__':
    api_key = "esoteric-code-546"
    query = 'blue bottle'
    service_url = 'https://www.googleapis.com/freebase/v1/search'
    params = {
        'query': query,
        #    'key': api_key
    }

    url = service_url + '?' + urllib.parse.urlencode(params)
    response = json.loads(urllib.request.urlopen(url).read())
    for result in response['result']:
        print("{name} ( {score} )".format(
            name=result['name'], score=str(result['score'])))
